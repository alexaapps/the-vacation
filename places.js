module.exports = {
    'Summer': {
        "Shillong": {
            "State": "Meghalaya",
            "Description": "The beautiful mountains of this region are a joy to watch. There are coal mines that can be spotted, legal and illegal, while you travel to Cherrapunjee from Shillong. Shillong in itself, is a really beautiful town, the capital of Meghalaya as well. Scenic viewpoints, caves en route to Cherrapunjee, views of Bangladesh from both Cherrapunjee and Mawlynnong, Asia’s cleanest village. These are just some of the things that will make your jaw drop. ",
            "Time": "Summers are pleasant in Shillong. The temperature during this time stays between 15 to 24 degrees Celsius. This is the best time to visit Shillong. "
        },

        "Manali": {
            "State": "Himachal Pradesh",
            "Description": " Manali never fails to make it to any list about beating the heat of May. Forever charming, this place never fails to make you go awestruck. Thick forests and cool breeze characterize the hill station. Major attractions include Hadimba Temple, Vashisth, monastries, Solang Valley, Rohtang Pass and Club House. ",
            "Time": "Summer steps in from March but the weather is still pleasant. The temperatures during these summer months usually vary between 10°C to 25°C, so you might still need light woolens during the night. This is the perfect time to indulge in outdoor activities such as paragliding, rafting, trekking and mountain adventures in the Solang Valley. This is also the time when the region’s flora is in full bloom lending it the most pristine natural beauty. "

        },

        "Tawang": {
            "State": "Arunachal Pradesh",
            "Description": "Situated at a height of 10000 feet, Tawang‘s beauty reaches out to your soul. With Tibet to its northern side, Sela range of West Kameng to the east, and Bhutan to the southwest, Tawang is beautifully located. Breathtaking valleys, amazing waterfalls and misty rivers add to the charm. Tawang Monastery, Sela Pass, Shonga-tser Lake, Nuranang waterfalls and Bumla Pass are places that deserve a visit in May in India, when in Tawang. ",
            "Time": "The best season to visit Tawang is the summers season and onset of monsoon season. The temperature remains comfortable and is apt for sightseeing. The best months to visit are March, April, May, June, September and October. "
        },

        " Dras Nubra Valley": {
            "State": "Jammu and Kashmir",
            "Description": " The Gateway to Ladakh, Drass is the second coldest inhabited region in the world. Drass valley starts from the base of Zoji-la Pass, from where you can take a 3-day long trek in May and visit places like Mushko Valley, Dras War Memorial and Draupadi Kund. Nubra Valley offers any traveller the sights of their lifetime. Wide landscapes, mystical Shyok river and the gorgeous mountain faces are enough to take you into a different world altogether. ",
            "Time": "The best season to visit Nubra Valley is the summer season. The temperature remains comfortable and is apt for sightseeing. The best months to visit are February to April and August to November. Nubra Valley experiences winters in the months of October to May. "
        },

        "Tirthan Valley ": {
            "State": "Himachal Pradesh",
            "Description": "This place is a plethora of delights. A paradise for nature lovers, Tirthan Valley is located nearly 3 kms from the entrance of Great Himalayan National Park. Adventurous activities are in abundance, something you expect from Himachal now. Famous for trout fishing and surprisingly high influx of foreigners, the place has much to offer. ",
            "Time": "March to June and October to November are the best months to visit Tirthan Valley. January-February is also good for those who are looking for snow and can handle extremely cold conditions. It is in full bloom during the spring season. "
        },
        "Ranikhet" : {
            "State" : "Uttarkhand",
            "Description" : "A perfect escape from the scorching heat of the summer, Ranikhet holds a charismatic layer of grasses dotted with wildflowers. Home to military quarters, peaceful parks dotted kaleidoscope of flowers, and gorgeous temples, Ranikhet is one of the laid back hill station of North India, situated in the state of uttarakhand , that is hardly explored by tourists. Those who want to relax, rejuvenate and love to live in the harmony with nature, should visit Ranikhet. ",
            "Time" : "Ranikhet is a beautiful and pleasant place throughout the year and can be visited anytime. The winters can treat you with snowfall while monsoon with fresher and stunning views. The monsoons leave the area as a host to many cultural and sports activities. "
        },

        "Saputara" : {
            "State" : "Gujarat",
            "Description" : "Not many people in India know that Gujarat has a hill station within its vicinity. Sapatura, a tiny hill station snuggled in the enchanting greens of Dang forest and ensconced in the Sahyadri range, is a little hub for mostly people living in West India. Saputara has an influence of India’s best Hill Station- luscious forest reserves of Chail, sunset points from Nainital, botanical gardens from Ranikhet, and waterfalls from Kerala. It’s a fantastic place to look out for nature’s bounty and spend some good playing time with your kids. ",
            "Time" : "Located at an altitude of 1000 meters above sea level, Saputara enjoys a cool and pleasant climate throughout the year. The average maximum and minimum temperatures recorded are 30 degrees Celcius and 10 degrees Celcius respectively. Saputara experiences moderate temperature in summer with the maximum temperature reaching around 28 degrees Celsius. Thus, Satpura is a favourable destination for summer holidays, owing to its cool climate.  "
        },
        "Sarahan" : {
            "State" : "Himachal Pradesh",
            "Description" : "Sarahan, a beautiful hamlet located in the Sutlej Valley, is situated at an average height of 2165 metres above sea level. It comes under the Shimla district and is famous for its apple orchards, pine forests, slate roofed houses and small streams. Bhimkali Temple Complex, Bhaba Valley and Bird Park are some of the major attractions here.  ",
            "Time" : "Summer months ranging from March through June is perhaps the best time to visit Sarahan. Owing to its cool and pleasant weather, the region serves as a perfect summer retreat. Tourists flock here during the summer season to escape the blistering heat prevalent in most parts of the country. Alternatively, you can also visit during September or October. The Astami Puja which takes place in October, two days before Dussehra also draws a considerable number of visitors to Sarahan. "
        }
    },
    "Winter": {
        "Gulmarg": {
            "Description": "Gulmarg is one of the places to visit in India in winters, this popular hill station of Kashmir is well known for delivering out the best skiing experience to the tourists. The Snow covered mountains; beautiful landscapes and the Gulmarg Gondola are something that has to be explored for sure at Kashmir. Gulmarg is a best skiing place, don’t forget your show your skiing skill here as you ski through Aparwath peak. ",
            "State": "Jammu and Kashmir",
            "Time": "If you are looking  for the USP of gulmar, that is skiing, the months of december to mid march are recommended to best enjoy the palce at all its glory. "
        },

        "Ladakh": {
            "State": "Jammu and Kashmir",
            "Description": " Ladakh is one of the best places to see snow in India, travelling to this place during winter will give you the best snowfall experience, the steep mountains and the snow covered paths are best when seen directly. The Chadar trek connects the villages of Zanskar valley with Chilling. Trekking here is considered to be a unique and exciting trek. ",
            "Time": "Though many visit ladakh during summers, Winter in Ladakh is a celebration time. Locals have very little to do during winter months, so free time is turned into celebration time.  Winter festivals are of different league. Ladakhi Losar, Spituk Gustor, Thiksay Gustor, Stok Monastery Festival, Matho Monastery festival, Dosmoche all fall in Ladakh in winters. The months of mid february to mid march are recommended. "

        },

        "Horsley Hills": {
            "State": "Andhra Pradesh",
            "Description": "One of the most scenic places in the state, the hills provide a good break from the town and its heat. With plenty of time and ways to be with nature, Horsley Hills are a paradise. The way to the hills is panoramic too,  with Mahogany, Jacaranda, Sandalwood, Gulmohar and Eucalyptus trees lining the road. Just when you begin to feel life has slowed down too much since you’ve come here, brace yourself for the lesser known thing about this place  adventure sports, which includes zorbing, rappelling, rock climbing and trekking. ",
            "Time": " The temperature during winter varies from 10- 20 degrees, hence the months between December to March are considered the best for visiting Horsely. The region experiences warm summers with mercury rising upto 30-35 degree celsius. "
        },
        "Tsomgo Lake" : {
            "State" : "Sikkim",
            "Description" : "Tsomogo Lake is one of the few places to visit in India in winters; the lake looks double attractive with its amazing surroundings and hill reflections. Further up you can see the Kyongnosla Alphine sanctuary which is the shelter for the Himalayan black bears, birds, Red panda and some rare animals. Visit the Tseten Tashi Cave on your route to Tsomgo Lake. ",
            "Time" : "The lake gets frozen in winters and it is the time when tourists can best enjoy a Yak safari on snow. Weather is often pretty unpredictable at Tsmogo Lake and the best time to visit the lake is between April to September as it remains either completely or partially frozen during other months. "
        },
        "Sonmarg" : {
            "State" : "Jammu and Kashmir",
            "Description" : "Summer may be good but if you want some places to see snow in India,  Sonamarg, situated in kashmir,  is the ideal snow vacation destination. Sonamarg has its own charm when it comes to meadow charm. On this trip you have to visit some places like Thajiwas Glacier, Zojila pass, Gangabal Lake, and accept a challenge to take the difficult Nichainai pass trek. ",
            "Time" : "Best time to visit Sonamarg is the period extending from March to August. During the winters, the area is heavily heaped with snow, hiding the NH 1D highway from Srinagar Airport to Sonamarg. For people who want to enjoy the snow, ideal time for visit is from November to February. "
        },
        "Narkanda" : {
            "State" : "Himachal Pradesh",
            "Description" : " Narkanda, situated in himachal pradesh offers bundle of things to do for the tourists, Take up a relaxing stroll towards the town and admire the scenic beauty or mediate for some time at the serene places. If you are adventurous you can even try out skiing and trekking. Sightseeing over here is all about visits to pine, spruce forests, apple orchards, paying a visit to temple, shopping of some shawls, woven carpets, blankets, Pullans. Also visit Kotgarh and Thanedhar to eat some fresh apples. ",
            "Time" : "The best time to visit Narkanda is during the summers when the weather is delightful, and the temperature ranges from 10 to 30 degrees Celcius. The winters are freezing and chilly and the temperature drops to around -10 degrees Celcius. People interested in skiing should visit during this season.  "
        },
        "Nainital" : {
            "State" : "Uttarakhand",
            "Description" : " The peace you get at home can be experienced at Nainital, Situated in uttarakhand, this Lake city secures the top place in the list of places to visit in India in winters. The sparkling lakes and the sprawling scenery will mesmerize you every second. The best thing to do is to eat, shop and sit and relax beside the Lake",
            "Time" : "Nainital is a year-round destination, but the ideal time to visit is between the months of March to June. However, depending on what you want to do, October can also be a good time to visit Nainital as it is neither too hot nor too cold during the day.  November marks the onset of winter season in Nainital and continues till February. This season has a beautiful misty charm with the coldest days seeing the temperature dipping to zero degree Celsius. If you like snow, then plan a trip between December-end and January. "
        },
        "Auli" : {
            "State" : "uttarakhand",
            "Description" : "Auli is a beautiful hill destination, nestled in the picturesque northern Indian state of Uttrakhand. Although only recently been growing in popularity, Auli is one of the favorite skiing spot for tourists, both national and international. In winters, the place gets a bountiful snow and together with enveloping mountains, consummate with oak and deodar trees, provides one of the most spectacular views. ",
            "Time" : "Tourist attraction in Auli is Snow Skiing and is best during November to March. May to November offers cool and pleasant climate and the right period to spend relaxing moments in the outdoors. If you love skiing, best time to visit Auli is January and February. "
        }

    },
    "Monsoon": {
        "Lonavala": {
            "State": "Maharashtra",
            "Description": "With the onset of monsoon, the Sahyadri mountain ranges and the ghats revive with alluring greenery, breath taking waterfalls and pleasant climate. For a quick escape from the bustling city, plan a trip to the quaint hill town of lonavala, situated 80 kilometers from mumbai. ",
            "Time": " The best time to visit here is from ending of spetember till the month of may, when the weather is pleasant and you can enjoy many activities and can admire the beauty of nature. It is best visited during the month of september. "
        },

        "Kodaikanal": {
            "State": "Tamil Nadu",
            "Description": "Known as the “princess of hill stations” this is one of the best monsoon destinations in India. Situated in the Palani hills of western ghat it offers enchanting waterfalls, lakes and lush greenery and spectacular views of the ghats and hills. ",
            "Time": "Kodaikanal is a famous and finest hill station in Tamil Nadu. Kodaikanal can be visited any time of the year, but the best time to visit is September to May; when the weather is at its best. "
        },

        "Coorg": {
            "State": "Karnataka",
            "Description": "One of the most beautiful monsoon destinations in India, Coorg is the not just one of the most beautiful hill stations of Karnataka but also India. Blessed with gorgeous scenery dotted with vast coffee plantations, it is the perfect getaway in the monsoon. During the rains, the Abbey and Jog waterfalls flow in their full might, creating an impressive picture. Also, if you are into adventure sports, then embark on a trek to the highest peak of Tadiandamol, which is known for its wonderful scenery. Bylekuppe, Madikeri Fort and other sights are equally pleasant. ",
            "Time": "Coorg is an evergreen place. No matter which time of the year to choose to spend your holidays here, the place will leave you breathless with the pleasant climate that this hill town offers all around the year. Although, the weather differs in different seasons of the year. Coorg receives a good amount of rainfall during the rainy season. The rains give a green and fresh look to the place. The coffee and tea plantations look mesmerising with the tiny droplets of rain covering the green fields. The temperature ranges between 23°C - 29°C. "
        },
        "Udaipur": {
            "State": "Rajasthan",
            "Description": "Udaipur in Rajasthan is next on the list of best places to visit in India during monsoon. It receives one of the lowest rainfalls in the country, just enough rains for a pleasant climate. The city is snuggled in the lap of the Aravalis and is dotted with a number of lakes. Titled as one of the ‘most romantic destinations in India’, imagine the romance that is possible during the rains. Visit the famous Monsoon Palace, and Sajjan Garh, which offers a panoramic view of the city. Visit the scenic Lake Pichola and the Fateh Sagar Lake, go for boat rides, and enjoy the popular sights and the royal hospitality. ",
            "Time": "The best time to visit Udaipur is from September till March as the weather remain favorable making it ideal for sightseeing and outdoor activities. Monsoon is yet another time of the year when one could go to Udaipur to explore lakes replenished by rains and find attractive deals of hotels and packages. "
        },
        "Alleppey": {
            "State": "Kerala",
            "Description": "Although a relatively small island, Alleppey or Alappuzha stands tall amongst the best tourist spots in Kerala. The reason isn’t difficult to fathom once you’ve witnessed the flawless beauty here. Often called the ‘Venice of the East’, owing to its serpentine network of canals and backwaters, Alleppey has grown from being a small town in Kerala to one of its biggest attractions. Much talked about, and not without reason, are the resplendent backwaters of Alleppey. One of the must-do things in Alleppey is to board one of the many magnificent houseboats and go on a cruise of utter pleasure and romance.  ",
            "Time": "Alleppey is divine during the monsoon. June marks the beginning of the monsoon season in Allepey and is a great time to enjoy this little town in its rain-washed glory. The rain makes the backwaters all the more beautiful. To explore this spectacular place, you must embark on a backwater cruise through its lakes, river and canals. Monsoon also enhances the benefits of Ayurveda. According to the ancient traditions of Ayurveda, the best time to undergo these treatments is during the monsoons. The weather is perfect, moist, cool and dust-free, opening up the pores of our skin, making the therapy more effective. The cool salubrious monsoon weather also allows the body to recuperate in bliss. "
        },
        "Orchha": {
            "State": "Madhya Pradesh",
            "Description": "Orchha is the erstwhile capital city of the Bundela rulers. The town is steeped in history and is famous for its palaces and temples built in the 16th and 17th centuries. The architectural splendor of the monuments in Orchha reflects the glory of its rulers. The Betwa River, on whose banks Orchha lies, and the forests around it attract tourist to this place. ",
            "Time": "Orchha is the perfect getaway to enjoy the magic of the monsoons. The season of rains turn this otherwise dry corner of Malwa plateau into the greenest and the most romantic spots on earth.  The Betwa River is revived by rain and the town settled on its banks is blessed with picturesque scenery of lush greenery, rolling hills, and serene bliss. Not just charming scenery, historical forts, places, temples, monuments, memorials, and heritage re-create the nostalgia of its golden period. Freshly washed, the buildings and the town simple look divine. If you are in love with rains, Orchha is one of the spectacular destinations to visit during monsoon"
        },
        "Agumbe": {
            "State": "Karnataka",
            "Description": "Agumbe is one of the most popular treks in South India especially in August. The mountains and valleys are filled with numerous waterfalls which are a prime attraction; there are also ruins of temples dating back to the Hoysala Empire which is worth a visit. There is a gallery erected on a high point of the hills to view the sunset and you can see as far as the Arabian Sea. ",
            "Time": "The best time to visit the place is during monsoon in the month of august, when the hill station is full of sparkling streams and several waterfalls.  "
        },
        "Mussoorie" : {
            "State" : "Uttarakhand",
            "Description" : " Nestled in the foothills of the Garhwal Himalayan ranges, Mussoorie is quite a scene in August in India. The drive from Dehradun to Mussoorie itself is breathtaking, add to that the undulating hills of Mussoorie and the spectacular view of the Himalayas, you have for yourself a holiday of perfectly quiet ambience. Waterfalls like Battha and Kempty are worth visiting and so is the Naga Devta Temple along with some beautiful churches ",
            "Time" : "Mussoorie is a most preferred summer retreat in the country where summer is the best time to trek and camp. Winter is freezing and is dominated by snowfall. Post monsoon season during September to November is good for water rafting. The type of weather of this place is cool and enjoyable throughout the year but the best time to visit Mussoorie is from the month of September to June. "
        }
    }
    ,
    "Autumn": {
        "Hampi": {
            "State": "Karnataka",
            "Description": "There is no other place in the world like Hampi. The city was once home to the wealthy Vijayanagara Empire in the 14th century and attracted traders and travellers from all over the world. Although the once formidable city now lies in ruins, its magnetism is still inescapable. The huge complex is dotted with the remains of temples, shrines, memorials, baths and other structures all cast out of enormous boulders. The ethereal landscape has the power to transport you to a whole different era, which almost feels meditative. ",
            "Time": " Cooler months of October to March are the peak season for Hampi. "
        },

        "Wayanad": {
            "State": "Kerala",
            "Description": "Wayanad in Kerala is considered by many to be the most beautiful destination in the state. The emerald landscape of the place is yet unspoilt by tourist invasion and makes for a great spot to relax and rejuvenate, far away from the frantic city life. You can hike through paddy fields and coffee plantations, visit wildlife sanctuaries, such as the Tholpetty Wildlife Sanctuary, and the ancient Edakkal Caves or simply repose on the shores of the stunning Pookode Lake with just your thoughts for company. The best hiking trails in Wayanad are in Chembra Peak, Pakshipathalam and Banasura Hill. Fresh off the heavy monsoon rains, the green terrains of Wayanad look even more welcoming during autumn. ",
            "Time": "October to May are preferred to visit Wayanad, since, the season is ideal for sightseeing, wildlife trips, video and photography. June to September are good for nature lovers as the greenery is at its best and short trips are also possible during these months. "
        },

        "Lahaul and Spiti": {
            "State": "Himachal Pradesh",
            "Description": "Lahaul and Spiti is a high altitude district in Himachal Pradesh. The place is famous for its various high altitude trek trails and a rich Tibetan influenced culture. Being home to a huge list of monasteries, flora, and fauna and various high mountain passes and rivers, Lahaul and Spiti is a major tourist attraction. ",
            "Time": "The fact that you can only travel to Lahaul and Spiti before the onset of winter makes them the best place to travel in October in India. njoy the wide and variety of flora and fauna found in Pin Valley National Park, catch the breathtaking view of the Milky Way galaxy lying under stars, and take the challenge of completing the Spiti Trek all at Lahaul and Spiti. Because nothing is as beautiful as the unscathed beauty of nature"
        },
        "Bandhavgarh National Park": {
            "State": "Madhya Pradesh",
            "Description": "Nothing matches the eternal bliss of watching fierce animals in their natural habitat. Bandhavgarh National Park in Madhya Pradesh is home to some rare species of flora and fauna, which puts it among the best places in India to visit in October. Apart from the rich biodiversity, the park boasts of a good number of Bengal Tigers in the world. Come October, a trip to Bandhavgarh is in the reckoning. ",
            "Time": "The best time to visit the park is during the fall in october. the peak season for Bandhavgarh National Park is during winters which is October to March. Most of the tourists visit the park between November and March, mainly because the summers heat is unbearable. "
        },
        "Pachmarhi": {
            "State": "Madhya Pradesh",
            "Description": "The queen of Satpura is one of the most popular hill station in madhya pradesh, and also a primary holiday destination in India in October. Sitting pretty at an altitude of 1100 meters above sea level, Pachmarhi lies beautifully entangled amid lush green nature, ancient caves, and waterfalls exuding beads of life.  ",
            "Time": "Pachmarhi can be visited throughout the year, though the best time to visit Pachmarhi is October to June. The summers are pleasant followed by moderate showers during the monsoon which leave the weather very attractive and enjoyable . "
        },
        "Digha": {
            "State": "West Bengal",
            "Description": "This romantic holiday beach destination in West Bengal is your answer to where to go in India in October. Also known as the Brighton of the East, Digha has shallow sand beaches that greet its visitors with mesmerizing sea waves. Dotted with casuarinas plantations along the coast, Digha also presents beautiful sunrise and sunset views to the romantic couples who flock to the place to especially catch the breathtaking shades of nature. ",
            "Time": "In the months of October to February, the winter season is witnessed in Digha. The temperature ranges between 3 to 24 degrees Celcius. The weather is pleasantly cool. This is considered to be the best time to visit Digha.  "
        },

        "Lachen": {
            "State": "Sikkim",
            "Description": "The scenic mountain village is home to Lanchen pass where lives the distinct Sikkimese-Bhutia community. Another important addition to the best places to visit in India in October, Lanchen exhibits the best that nature could probably offer. From snow-capped mountains to grass-clad slopes, and from trails leading to country’s highest mountain peak to the valley painted by different hues of flowers, Lanchen has it all. Nature trek, local tradition, and few sumptuous local cuisines on your platter make this place another alluring prospect where you could plan your next visit. ",
            "Time": "The ones, who are planning a trip to Lachen, must know that the best time to visit this place begins from October and lasts till June. Summers are quite pleasant here, opening up chances for sightseeing and several other outdoor activities. Monsoon however is not considered to be a good season to visit Lachen, as heavy rainfall disrupts travelling. Winter season is a bit chilly, yet is a great season to witness the spell-binding views of the mountains. "
        },
        " Rishikesh": {
            "State": "Uttarakhand",
            "Description": "Sitting pretty on the northern frontier of the Himalayas is Rishikesh; a holy city that thrives by the banks of the Ganges. There are quite a few religious places and plenty of adventurous activities that make Rishikesh an ideal holiday destination in India in October. Take a dip at the Triveni Ghat- the confluence of Ganga, Yamuna, and Saraswati, and wash all your sins because it’s time to begin afresh!",
            "Time": "The best time to visit Rishikesh for River Rafting is from late September to mid November, and then the best time to visit Rishikesh is from early March to first week of May. "
        },
    },
    "Spring": {
        "Gangtok": {
            "State": "Sikkim",
            "Description": "In the enchanting hills of northeast India lay an amazingly beautiful state called Sikkim. The state is known for its lip smacking food, Buddhism, spirituality and amazing eye pleasing Himalayan Mountains. Gangtok in Sikkim makes the perfect place to enjoy all these with the comfort cool winds of spring season. ",
            "Time": "During its peak season, that is from September to June, Gangtok draws in many tourists, Still if you inquire the best time to visit Gangtok then the bracing autumn that is the months of September and October and from Spring to summers that is from March to June is the ideal time. Well one who is planning a crazy and adventurous trip to Gangtok during the monsoon should be aware of landslides, which is often devastating. "
        },

        "Coonoor": {
            "State": "Tamilnadu",
            "Description": "Coonoor, Ooty and Kotagiri are the three major hill stations in the beautiful region of Nilgiri hills in the state of Tamil Nadu. The place is comfortably warm during the spring season that makes it a perfect place to unwind and welcome the spring season. ",
            "Time": "The period from October till March is perhaps the best time to visit a hill station like Coonoor. The greenery, the tea plantation, the clear skies and a hint of morning mist make the place appear magical to the travellers. The place is cold at this time, yet pleasant and offers all the more enjoyment. One needs to carry some woollens as nighttime temperatures can get low. "
        },

        "Thekkady": {
            "State": "Kerala",
            "Description": "By the end of February mango plants are in full bloom in Kerala. Thekkady is a small town in Kerala that is best known for its lush fields. The adjacent Periyar National Park is the ideal place to be at this time. This is the time when many animals emerge from within the forests. ",
            "TIme": " The on set of spring is the best time to visit Thekkady as the weather is cool and pleasant, making it ideal for sightseeing. The maximum temperature goes up to 15 degrees Celsius and this is a great time to go for wildlife safaris and admire the natural beauty of Thekkady. Mid february to mid march is widely preferred. "
        },
        "Andaman and Nicobar islands": {
            "State": "Andaman and Nicobar Union Territory",
            "Description": "A group of nearly 570 islands, this exotic location has amazing wildlife, thrilling water sports, pristine silver sand beaches, mountains, breath taking natural beauty and tribal tour. This place will never seize to amaze you and you are sure to fall in love with its incredible beauty. it has to be a must on your list of best monsoon destinations in India. ",
            "Time": "Andaman and Nicobar Island is a year-round tourist destination. The area receives minimum fluctuations in the temperature and is always pleasant. Best time to visit Andamana and Nicobar Island is considered from November to mid-May. But the island is best enjoyed during the monsoons. "
        },

        "Ranthambore": {
            "State": "Rajasthan",
            "Description": "Let’s go tiger spotting into the wild! Located near the town of Sawai Madhopur in Rajasthan, Ranthambore National Park is one of the renowned wildlife reserves in India. If you are a wildlife photographer or an adventure seeker, you can head to Ranthambore to catch a glimpse of tigers and other wildlife animals. ",
            "Time": "October to March is the best time to visit Ranthambore. The cool weather and blooming flowers add gloss to the trip. However, please carry heavy woollens if you plan to travel in the month of December or early January. "
        },

        "Munnar": {
            "State": "Kerala",
            "Description": "If you couldn’t take a break from the city life in winters, spring is the perfect time to escape the life’s monotony and take a trip to Munnar. Take a walk through the aromatic tea gardens, majestic waterfalls and winding undulating paths, and you will have a delightful time in the overwhelming beauty of this hill station. ",
            "Time": "Late january to early march is the best time to visit munnar.  The minimum temperature during this time of the year drops to ten degrees Celsius and is perfect for a vacation or a honeymoon. This is also a great time for adventure enthusiasts who can enjoy adventure activities such as rock climbing, trekking and rappelling. "
        },
        "Kovalam ": {
            "State": "Kerala",
            "Description": "Once a calm fishing village, Kovalam is now a major tourist resort in Kerala. With the shallow waters, pristine beaches and white sand, this beach town makes for an ideal destination during spring. Here you can indulge in water sports and recreational activities, or spend time lazing at the beach. ",
            "Time": "January to mid march is by far the best time to visit Kovalam as the temperature is moderate and conducive for sightseeing, beach hopping, or honeymooning. The minimum temperature during this time of the year is 16 degrees Celsius and the maximum temperature goes up to 34 degrees Celsius. "
        },
        "Rhododendron Sanctuary": {
            "State": "Sikkim",
            "Description": "Sikkim is a beautiful state, but it is the most in the months of April and May. As you can see subtle pink flowers called the rhododendrons blossom taking the beauty of the place at level beyond comprehension. You can go for a trek and relish the view of these beautiful flowers, as it is on of best sights during Spring season in India. ",
            "Time": " the best time to visit rhododendron is March to May, when the flowers that give this sanctuary its name are in bloom, dotting the forested mountainsides with pink, red and white. "
        },
        "Kasol": {
            "State": "Himachal Pradesh",
            "Description": "Kasol is a suburb located on the banks of River Parvati in the peaceful abode of Himachal Pradesh in North India. This quiet and tranquil place is nested in the Himalayas and is much more than a delight for all the bag packers. It is an ideal destination for a two-three day getaway for those of you that love snow. Picturesque locations around this surreal place has some delightful tourist destinations which can be ticked off easily. Situated amidst a number of other scenic destinations, Kasol is an easy going yet a thrilling tourist spot for offbeat destination lovers. ",
            "Time": "Visiting Kasol in winters isn’t really a good idea,  since temperatures can drop to below 10 degrees and keep you stuck by your heater instead of having fun outdoors. However, visit in summer and you might just miss the point of going there. The sweet spot for planning a Kasol trip is always during the spring months of March to June when the temperature remains a pleasant 15-20 degrees and allows you take in the breathtaking beauty of Kasol without saddling yourself with four layers of wool!"
        },
        "Mon": {
            "State": "Nagaland",
            "Description": "The land of Konyak Nagas, Mon is an interesting land to visit in Nagaland. The Konyaks call themselves the descendants of Noah and practice agriculture; it is believed that they have the finest harvest in the state. The chief’s house, where the chief resides with his 60 wives, is a major attraction in Longwa. The large number of trophies made of animals’ skull in the residence is some unforgettable sight. Shangnyu Village and its famed wooden carving, Veda Peak and Chui Village are some of the other attractions in Mon. ",
            "Time": "Apart from the favourable weather and pristine natural beauty, the best reason to visit Mon during the spring season, especially in the month of April is the Aoleang Festival of the Konyak tribe. The festival itself celebrates the onset of spring and usually takes place during the first week of April. The first three days are associated with traditional weaving and brewing of the famous rice beer of Nagaland, while the fourth day sees tribal dance performances and sacrificial rituals.  "
        }
    }
}