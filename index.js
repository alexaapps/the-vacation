var place = require('./places.js') // importing the JS file into place//
const Alexa = require('alexa-sdk');
var dateobj = new Date();
var month = dateobj.getMonth() + 1;
var day = dateobj.getDate();
var year = dateobj.getFullYear();
console.log(month);


exports.handler = function (event, context, callback) {
    const alexa = Alexa.handler(event, context, callback);
    alexa.registerHandlers(handlers);
    alexa.appId = "amzn1.ask.skill.5e9a2e68-2572-481f-be7e-33dd41fa8cc5"; // APP_ID is your skill id which can be found in the Amazon developer console where you create the skill.
    alexa.execute();
};

const handlers = {
    "LaunchRequest": function () {
        console.log("Inside launch request");

        var i = 0;
        this.attributes.i = i;

        this.response.speak("Hello There ! Lets make sure you are onto a great vacation. May i know the month or season in which you plan to have a vacation ?")
            .listen('Say, tell me the best places to visit during monsoon in India.')
            .shouldEndSession(false);
        this.emit(":responseReady");
    },


    'SeasonIntent': function () {
        if (typeof this.event.request.intent.slots.season.value != 'undefined') {

            console.log("inside season intent");
            var speechText = '';
            var temp;

            for (var key in place) {
                console.log("inside for loop" + key)
                var d = key;
                var t = this.event.request.intent.slots.season.resolutions.resolutionsPerAuthority[0].values[0].value.id;
                if (t.toLowerCase() == d.toLowerCase()) {
                    for (var loc in place[key]) {
                        console.log(loc);
                        var temp = `${loc}`;
                        speechText += `${loc}, `;
                    };
                    console.log(speechText);
                }
            };
            console.log(speechText);
            if (speechText == undefined || speechText == '') {
                var i = 1;
                this.attributes.i = i;
                this.response.speak("Sorry, I did not get you, can you please repeat the season ? ")
                    .listen('During which season do you want to plan your vacation?')
                    .shouldEndSession(false);
                this.emit(':responseReady');

            }
            else {
                var i = 2;
                this.attributes.i = i;
                this.response.speak(`The best vacation spots to visit during  ${t}   are,  ${speechText}  You may ask alexa, to tell more about any of these places. For example, say, alexa, tell me about ${temp} or what is the best time to visit ${temp}`)
                    .listen('You may ask, tell me about horsley hills. ')
                    .shouldEndSession(false);
                this.emit(':responseReady');

            }

        }

        else {

            this.response.speak('Sorry! I did not get you. Can you please repeat it.')
                .listen('Try, Suggest me palces to visit in India, during the month of March')
                .shouldEndSession(false);
            this.emit(':responseReady');
        }

    },

    'MonthIntent': function () {
        if (typeof this.event.request.intent.slots.month.value != 'undefined') {

            var speechText = '';
            var sort;
            var temp;
            var c = this.event.request.intent.slots.month.resolutions.resolutionsPerAuthority[0].values[0].value.id;
            console.log(c);
            var m = c.replace(/\s/g, '');
            // var k = key.replace(/\s/g, '');
            //  console.log(k + "and " + t);
            // console.log(key);
            var ar = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];
            for (t in ar) {
                if (ar[t] == m.toLowerCase()) {
                    console.log(ar[t]);
                    if (ar[t] == 'january') {
                        for (var loc in place['Winter']) {
                            speechText += `${loc}, `;
                            sort = "january";
                            temp = `${loc}`;
                        };

                    }
                    if (ar[t] == 'february') {
                        for (var loc in place['Winter']) {
                            speechText += `${loc}, `;
                            sort = "february";
                            temp = `${loc}`;
                        };

                    }
                    if (ar[t] == 'march') {
                        for (var loc in place['Spring']) {
                            speechText += `${loc}, `;
                            sort = "march";
                            temp = `${loc}`;
                        };
                    }
                    if (ar[t] == 'april') {
                        for (var loc in place['Summer']) {
                            speechText += `${loc}, `;
                            sort = "april";
                            temp = `${loc}`;
                        };

                    }
                    if (ar[t] == 'may') {
                        for (var loc in place['Summer']) {
                            speechText += `${loc}, `;
                            sort = "may";
                            temp = `${loc}`;
                        };

                    }
                    if (ar[t] == 'june') {
                        for (var loc in place['Summer']) {
                            speechText += `${loc}, `;
                            sort = 'june';
                            temp = `${loc}`;
                        };

                    }
                    if (ar[t] == 'july') {
                        for (var loc in place['Monsoon']) {
                            speechText += `${loc}, `;
                            sort = 'july';
                            temp = `${loc}`;
                        };

                    }
                    if (ar[t] == 'august') {
                        for (var loc in place['Monsoon']) {
                            speechText += `${loc}, `;
                            sort = 'august';
                            temp = `${loc}`;
                        };

                    }
                    if (ar[t] == 'september') {
                        for (var loc in place['Monsoon']) {
                            speechText += `${loc}, `;
                            sort = 'september';
                            temp = `${loc}`;
                        };

                    }
                    if (ar[t] == 'october') {
                        for (var loc in place['Autumn']) {
                            speechText += `${loc}, `;
                            sort = 'october';
                            temp = `${loc}`;
                        };

                    }
                    if (ar[t] == 'november') {
                        for (var loc in place['Autumn']) {
                            speechText += `${loc}, `;
                            sort = 'november';
                            temp = `${loc}`;
                        };

                    }
                    if (ar[t] == 'december') {
                        for (var loc in place['Winter']) {
                            speechText += `${loc}, `;
                            sort = 'december';
                            temp = `${loc}`;
                        };

                    }
                }
            };
            console.log(speechText);

            if (speechText == undefined || speechText == '') {
                var i = 3;
                this.attributes.i = i;
                this.response.speak("Sorry, I did not get you, can you please repeat the month.")
                    .listen('During which month are you planning a vacation ?')
                    .shouldEndSession(false);
                this.emit(':responseReady');
            } else {
                var i = 4;
                this.attributes.i = i;

                this.response.speak(`  The best places to hit during the month of ${sort} are, ${speechText}. You may ask alexa, tell me about ${temp} or what is the best time to visit ${temp} `)
                    .listen('Say, tell me about shillong')
                    .shouldEndSession(false);
                this.emit(':responseReady');

            }


        }
        else {

            this.response.speak('Sorry! I did not get you. Can you please repeat it.')
                .listen('Try, Suggest me palces to visit in India, during the month of March')
                .shouldEndSession(false);
            this.emit(':responseReady');

        }


    },

    'RightnowIntent': function () {
        var a = month;
        var sort;
        var hemp;
        var speechText = '';
        if (a == 1) {
            for (var loc in place['Winter']) {
                speechText += `${loc}, `;
                sort = "january";
                hemp = `${loc}`;
            };

        }
        if (a == 2) {
            for (var loc in place['Winter']) {
                speechText += `${loc}, `;
                sort = "february";
                hemp = `${loc}`;
            };

        }
        if (a == 3) {
            for (var loc in place['Spring']) {
                speechText += `${loc}, `;
                sort = "march";
                hemp = `${loc}`;
            };
        }
        if (a == 4) {
            for (var loc in place['Summer']) {
                speechText += `${loc}, `;
                sort = "april";
                hemp = `${loc}`;
            };

        }
        if (a == 5) {
            for (var loc in place['Summer']) {
                speechText += `${loc}, `;
                sort = "may";
                hemp = `${loc}`;
            };

        }
        if (a == 6) {
            for (var loc in place['Summer']) {
                speechText += `${loc}, `;
                sort = "june";
                hemp = `${loc}`;

            };

        }
        if (a == 7) {
            for (var loc in place['Monsoon']) {
                speechText += `${loc}, `;
                sort = "july";
                hemp = `${loc}`;
            };

        }
        if (a == 8) {
            for (var loc in place['Monsoon']) {
                speechText += `${loc}, `;
                sort = "august";
                hemp = `${loc}`;
            };

        }
        if (a == 9) {
            for (var loc in place['Monsoon']) {
                speechText += `${loc}, `;
                sort = "september";
                hemp = `${loc}`;
            };

        }
        if (a == 10) {
            for (var loc in place['Autumn']) {
                speechText += `${loc}, `;
                sort = "october";
                hemp = `${loc}`;
            };

        }
        if (a == 11) {
            for (var loc in place['Autumn']) {
                speechText += `${loc}, `;
                sort = "november";
                hemp = `${loc}`;
            };

        }
        if (a == 12) {
            for (var loc in place['Winter']) {
                speechText += `${loc}, `;
                sort = "december";
                hemp = `${loc}`;
            };

        }

        if (speechText == undefined || speechText == '') {
            var i = 5;
            this.attributes.i = i;

            this.response.speak("Sorry, I did not get you, can you please repeat the month.")
                .listen('During which month are you planning a vacation ?')
                .shouldEndSession(false);
            this.emit(':responseReady');
        }
        else {
            var i = 6;
            this.attributes.i = i;

            this.response.speak(` The best places to hit during the month of ${sort} are, ${speechText}. You may ask alexa, to tell more about any of these places. For example, ask alexa, tell me about ${hemp} or what is the best time to visit ${hemp} `)
                .listen('Anything else you want to know about ?')
                .shouldEndSession(false);
            this.emit(':responseReady');

        }
    },

    'DescriptionIntent': function () {
        if (typeof this.event.request.intent.slots.location.value != 'undefined') {
            console.log(typeof this.event.request.intent.slots.location.value)

            console.log("inside description intent");
            var speechText = '';
            for (key in place) {
                for (var loc in place[key]) {
                    console.log("inside for loop" + key)
                    var d = loc;
                    var l = d.replace(/\s/g, '');
                    var t = this.event.request.intent.slots.location.resolutions.resolutionsPerAuthority[0].values[0].value.id ;
                    var l2 = t.replace(/\s/g, '');
                    if (l.toLowerCase() == l2.toLowerCase()) {
                        console.log(loc);
                        speechText += `${place[key][loc].Description} `;
                    }
                };

            };

            console.log(speechText);
            if (speechText == undefined || speechText == '') {
                var i = 7;
                this.attributes.i = i;


                this.response.speak("Sorry, I did not get you, can you please repeat the name of the location ? ")
                    .listen('During which season do you want to plan your vacation?')
                    .shouldEndSession(false);
                this.emit(':responseReady');

            }
            else {
                var i = 8;
                this.attributes.i = i;

                this.response.speak(` ${speechText}  Any other place you want to know about ?`)
                    .listen('You may ask, tell me about horsley hills. ')
                    .shouldEndSession(false);
                this.emit(':responseReady');

            }



        }
        else {

            this.response.speak('Sorry! I did not get you. Can you please repeat it.')
                .listen('Try, Suggest me palces to visit in India, during the month of March')
                .shouldEndSession(false);
            this.emit(':responseReady');


        }

    },

    'TimingIntent': function () {
        if (typeof this.event.request.intent.slots.time.value != 'undefined') {
            console.log("inside timing intent");
            var speechText = '';
            var con;
            for (key in place) {
                for (var loc in place[key]) {
                    console.log("inside for loop" + key)
                    var q = loc;
                    var a = q.replace(/\s/g, '');
                    var t = this.event.request.intent.slots.time.resolutions.resolutionsPerAuthority[0].values[0].value.id;
                    var a2 = t.replace(/\s/g, '');
                    if (a.toLowerCase() == a2.toLowerCase()) {
                        console.log(loc);
                        speechText += `${place[key][loc].Time} `;
                        con = loc;
                    }
                    console.log(speechText);
                };

            };

            console.log(speechText);
            if (speechText == undefined || speechText == '') {
                var i = 9;
                this.attributes.i = i;

                this.response.speak("Sorry, I did not get you, can you please repeat the name of the location ? ")
                    .listen('Say, what is the best time to visit' + loc)
                    .shouldEndSession(false);
                this.emit(':responseReady');

            }
            else {
                var i = 10;
                this.attributes.i = i;

                this.response.speak(`${speechText}  Anything else you need to know about ?`)
                    .listen('You may ask, what is the best time to visit' + loc)
                    .shouldEndSession(false);
                this.emit(':responseReady');

            }





        }
        else {

            this.response.speak('Sorry! I did not get you. Can you please repeat it.')
                .listen('Try, what is the best time to visit kasol')
                .shouldEndSession(false);
            this.emit(':responseReady');

        }


    },

    'StateIntent': function () {
        if (this.event.request.intent.slots.state.value != 'undefined') {
            console.log("inside states intent");
            var speechText = '';
            for (key in place) {
                for (var loc in place[key]) {
                    console.log("inside for loop" + key)
                    var d = loc;
                    var l = d.replace(/\s/g, '');
                    var t = this.event.request.intent.slots.state.resolutions.resolutionsPerAuthority[0].values[0].value.id;
                    var l2 = t.replace(/\s/g, '');
                    if (l.toLowerCase() == l2.toLowerCase()) {
                        console.log(loc);
                        speechText += `${place[key][loc].State} `;
                    }
                    console.log(speechText);
                };

            };

            console.log(speechText);
            if (speechText == undefined || speechText == '') {
                var i = 11;
                this.attributes.i = i;


                this.response.speak("Sorry, I did not get you, can you please repeat the name of the location ? ")
                    .listen('During which season do you want to plan your vacation?')
                    .shouldEndSession(false);
                this.emit(':responseReady');

            }
            else {
                var i = 12;
                this.attributes.i = i;

                this.response.speak(`It is situated in the state of ${speechText}.  Any other place you want to know about ?`)
                    .listen('You may ask, where is horsley hills located ? ')
                    .shouldEndSession(false);
                this.emit(':responseReady');

            }

        }
        else {

            this.response.speak('Sorry! I did not get you. Can you please repeat it.')
                .listen('Try, which state does hampi belong to')
                .shouldEndSession(false);
            this.emit(':responseReady');

        }




    },





    "AMAZON.CancelIntent": function () {
        this.response.speak('Thanks for using the vacation. Have a great day!')
            .shouldEndSession(true);
        this.emit(':responseReady');
    },

    "AMAZON.HelpIntent": function () {
        this.emit('LaunchRequest');
    },

    "AMAZON.StopIntent": function () {
        this.response.speak('Thanks for using the vacation. Have a great day!')
            .shouldEndSession(true);
        this.emit(':responseReady');
    },
    "AMAZON.NoIntent": function () {
        this.response.speak('Thanks for using the vacation. Have a great day!')
            .shouldEndSession(true);
        this.emit(':responseReady');
    },

    "AMAZON.YesIntent": function () {
        this.response.speak('Tell the season or the month, you prefer to plan your vacation.')
            .shouldEndSession(false);
        this.emit(':responseReady');
    },



    'Unhandled': function () {
        this.response.speak('Sorry! I do not know that')
            .listen('Try, Suggest me palces to visit in India, during the month of March')
            .shouldEndSession(false);
        this.emit(':responseReady');
    }


}
