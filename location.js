module.exports = {
    'srinagar': {
        "Siteseeing": {
            "Dal Lake": {
                "Description": "Dal is a lake in Srinagar, the summer capital of Jammu and Kashmir. The urban lake, which is the second largest in the state, is integral to tourism and recreation in Kashmir and is named the Jewel in the crown of Kashmir or  Srinagar's Jewel.  "
            },
            "Nishat Bagh": {
                'Description': "Also known as “the garden of bliss”, Nishat Garden in Srinagar is set on the banks of the beautiful Dal Lake. The garden is set at the backdrop of the snow capped Pir Panjal mountain range and it offers a great sunset view of the Dal Lake. Also, a beautiful water channel flows right in the middle of the garden. "
            },
            "Chashme Shahi": {
                'Description': "The Chashme Shahi is one of the beautiful mughal garden built around a spring in the Zabarwan Range. A fresh water spring, terraces, water pool and a large fountain makes it the most charming of the gardens near Srinagar,  along the banks of the Dal lake. "
            },
            "Pari Mahal": {
                'Description': "Pari Mahal is beautiful seven terraced garden over-looking city of Srinagar at the top of Zabarwan mountain range in Srinagar. The gardens is the property of the Srinagar government and also known as the Worlds Beautiful High Altitude Paramilitary Camp."
            },
            "Verinag Garden": {
                'Description': "Verinag Garden lies at the entry point of Kashmir Valley in the summer capital of the state of Jammu & Kashmir and also the first tourist spot of Kashmir Valley. The Verinag Spring of Mughal and surrounding area is officially recognized by Archaeological Survey of India and a Monument of National Importance."
            }
        },

        "ReligiousPlaces": {
            "Shankaracharya Temple": {
                "Description": "One of the most revered Hindu pilgrimage destinations in Srinagar, the Shankaracharya Temple is located atop a hill known as 'Takht-e-Suleiman'. The ancient temple stands at a height of about 1100 feet above the surface level of the main Srinagar city. A visit to this sacred temple involves a trek to the top of the hills."
            },
            "KheerBhawani Temple": {
                "Description": "The Kheer Bhawani Temple in Srinagar is one of the important places of worship in Kashmir. The Kheer Bhawani Temple is associated with Ragnya Devi and located in Tulamula in Srinagar. An important Hindu shrine, it is visited by a number of Hindu pilgrims on Jesht Ashtami when the annual festival is held"
            },
            "Hazratbal Shrine": {
                "Description": "The Hazratbal Shrine is a Muslim shrine set on the left bank of the Dal Lake. The shrine is considered the holiest Muslim shrine in Kashmir valley. The shrine was actually built as a pleasure house by Sadiq Khan, Subedar of Shah Jahan. Today, the place is a well known shrine which is visited thousands of people every year. The shrine is considered extremely sacred among the Muslims."
            }
        },

        "Restaurants" : {
            "Ahdoos" : {
                "Description" : "If you want to try the mouth-watering and popular Wazwan cuisine head to Ahdoos restaurant. The preparation of the Wazwan dishes is an art in itself. The spices are specially made by the Waza (Kashmiri chef) and the food infused with the flavours of saffron, cardamom, cloves and cinnamon. It also offers sumptuous vegetarian cuisine."
            },

            "The Chinar" : {
                "Description" : "The location of the restaurant, overlooking the Valley, makes eating here an exotic experience. The food, although comparatively expensive, is a gastronomical treat."
            },
        }
    },
    "chandigarh": {
        "Siteseeing": {
            "Nek Chand's Rock Garden": {
                "Description": "This sprawling, maze-like park is laid out like a fantasy land, packed with narrow corridors, amphitheatres, bridges, staircases, a spectacular waterfall and an ethnic market at the end"
            },

            "Sukhna Lake": {
                "Description": "This artificial water body is a popular tourist spot, where you can go paddle boating or simply take in the view with a stroll along its promenade."
            },

            "Pinjore Garden": {
                "Description": "These restored Mughal-era gardens are spread across seven levels, interspersed with palaces, lawns, a zoo and even a Japanese garden. "
            },
            "Leisure Garden": {
                "Description": "Created by Le Corbusier in the eroded valley of a seasonal river, Leisure Valley is an 8km long park which consists of a series of theme gardens."
            },



        },

        "Restaurants": {
            "Swagath": {
                "Description": "It may not be the best looking restaurant around town, but it is one of the most popular, that too, serving as it does, the food from the opposite end of the country.",
                "Address": " SCO No. 7, Ground Floor, Madhya Marg, near P P Jewellers, Sector 26"
            },

            "Saffron": {
                "Description": "Try the Tandoori Chicken here, you'll never believe that you are not in some highway dhaba, eating under the stars,  it has the exact spicing of a more earthy eatery.",
                "Address": " JW Marriott Hotel, Mezzanine Floor, Plot No. 6, Dakshin MargIn JW Marriott Hotel, Sector 35 - B, Chandigarh"
            },

            "Backpackers": {
                "Description": "The only known all-day breakfast place in Chandigarh, Backpackers is a cheerful place that buzzes at all times of the day and night, and is visited by a variety of people, from the young and trendy to the young at heart.",
                "Address": "SCF No. 16, Inner Market, Near Punjab Store, Sector 9 D, Chandigarh"
            },

        },

        "Shopping": {
            "Sector 17": {
                "Description": "Chandigarh’s main shopping centre in Sector 17 comprises of four piazzas meeting at a central chowk. You can easily spend an entire day browsing through the numerous showrooms, local boutiques, departmental stores and government emporia here."
            },
        },

        "Religious Places": {
            "Chandi Mandir": {
                "Description": "15 kms from the city on the Chandigarh Kalka road, Chandi Mandir is dedicated to Chandi, the goddess of power. Chandi Devi temple near Chandigarh, India is basically a Siddh Peeth, a place where wishes are believed to come true. The city of Chandigarh was named after this temple. During the festivity of Navratras, thousands of people visit this temple. When it comes to Chandigarh excursions, visiting the Chandi Devi mandir is like a must. "
            },
            "Mansa Devi Temple" : {
                "Description" : "Chandigarh Mansa Devi temple is a very famous shrine located at Panchkula, which is about 8 kms away from the city. This holy Siddh Peeth is dedicated to Mata Mansa Devi"
            },
            
            "Nada Sahib Gurdwara" : {
                "Description" : "Situated at the Panchkula district on the banks of river Ghagar, it lies at a distance of 15 kms from the city of gardens.  it is here that Guru Gobind Singhji along with his victorious fellow warriors had taken a halt while on their way back, after having fought the battle with Mughals "
            },
            " Baoli Sahib Gurdwara" : {
                "Description" : "Gurdwara Baoli Sahib is a very famous shrine. Devotees come from all over the country to visit this religious place. It is this place where Guru Gobind Singhji visited and restored the water supply to quench the thirst of people of this area. his village is about 10 kms away from Chandigarh, situated on the Zirakpur-Kalka highway."
            }
        }
    },
    "shimla": {
        "Siteseeing": {
            " Summer Hills ": {
                "Description": "One of the best places to visit in Shimla is the Summer slope, a beautiful township is found 5 km from the well known edge of Shimla. This slope is covered by rich greenery that offers stunning perspectives from the top. Summer slope is likewise a part of the 7 slopes that make Shimla, so it is certain to abandon you overpowered by its excellence."
            },

            "The Shimla State Museum": {
                "Description": "The Shimla state gallery otherwise called the Himachal state historical center and library is situated on Mount Pleasant. The sprawling yards and the splendid British engineering makes this place absolutely beneficial. The exhibition hall incredibly shows the eminent past of the state with its rich culture and legacy. They have a mind boggling accumulation of various relics, compositions, models, crafted works and significantly more which they have been saving since ages."
            },

            "Chadwick Falls": {
                "Description": "One of the significant vacation destinations of Shimla, Chadwick falls, tumbles from a height of 1586 meters. With thick woodland, rich deodar and pine tree encompassing it, the view is thoroughly captivating. The storms prompt to an expansion in the water level of the falls making the place significantly more delightful with its shimmering water. Chadwick falls is clearly a treat to one's eyes with its enthralling excellence and vibe. "
            },
            "Jakhoo Slope": {
                "Description": "Accepted to be the most noteworthy top in Shimla, this slope is 8000 feet high and is a noteworthy vacation destination in Shimla. This sanctuary is a paradise for all the nature beaus and pioneers who come to visit the notable 108 feet tall Hanuman statue at the Jakhoo sanctuary. It is just about a short trek for experience searchers."
            },



        },

        "Restaurants": {
            " Embassy restaurant and café": {
                "Description": "The café with fabulous views over Shimla has interesting quotations from all sorts of literature, decorated by the owner. The menu runs to Indian, Chinese and continental, with good biryanis and home cooked cakes.",
            },

            "Ashiana and Goofa": {
                "Description": " Located at the Ridge on Mall Road, this Restaurant offers a majestic view of the snow-clad mountains on one side and the beautiful town of Shimla on the other. Whatever little can be had of Himachali cuisine can be found at these twin-restaurants run by Himachal Tourism, the one located above the other. ",
            },

            "The Devicos": {
                "Description": "One of the very few restaurants in town with food appealing both to Indian and to Western palates, this is one of the most expensive places to eat. The décor is very appealing as well.",
            },

        },

        "Shopping": {
            "Lakkar Bazaar": {
                "Description": "Adjoining the Ridge, Lakkar Bazaar is a marketplace which offers wooden articles targeted mainly towards tourists. The hand-crafted wooden items are truly worth the money and sell like hot cakes. Tourists take them back as a souvenir of their trip. Lakkar Bazaar is also famous for its dry fruits and natural herbs."
            },

            "Tibetan Market" : {
                "Description" : "This makeshift market is perched on Shimla’s slopes and is the perfect destination for those who love a good bargain. Woollen clothes, Tibetan rugs and carpets, padded jackets, mufflers, scarves, sweaters as well as quality Tibetan accessories like bags, boots, shoes and jewellery are found in its bylanes. Located just beneath the Ridge, the market is the preferred destination for travellers looking to shop at reasonable prices. Corn cobs swathed in butter, salt and lime are sold by wayside vendors and are a great snack to indulge in while you wander through the market."
            },

            "Himachal Emporium" : {
                "Description" : "If you’re in the mood to shop for locally made handicrafts at pocket friendly prices, Himachal Emporium is just the place for you. Located at the Mall Road in Shimla, the emporium offers handicraft products of Himachal Pradesh like locally designed woollens, pottery items and jewellery. There is an enormous range of Kangra silks, Kinnauri shawls and other Himachal souvenirs available here. Run by the Himachal Pradesh Government, the emporium sells genuine handicrafts for the discerning buyers."
            },

            "The Mall" : {
                "Description" : "In Shimla, the Mall Road lies at the heart of all the action and activity. Buzzing with shops, cafes, theatres, restaurants and a whole bunch of merriment, it is close to all places of interest. If you’re looking to do a spot of shopping, the Mall offers a number of emporiums, showrooms and storehouses selling everything from shawls and woollens to jewellery, pottery and books. "
            },
        },

        "Religious Places": {
            "Bhimakali Temple": {
                "Description": "The famous Bhimakali Temple in Shimla is a very popular tourist attraction. Also known as Bheemakali Temple of Simla, this architectural wonder is one of the representatives of 51 Shakti Peeths. The unique thing about this temple is that the style of architecture used to build it is a unique amalgamation of both Hinduism and Buddhism. It is built of wood and is no less than an architectural masterpiece. "
            },
            "Jakhu Temple" : {
                "Description" : "At the top of the famous Jakhu hill is located the famous Jakhu Temple of Shimla. It is hardly 2 kilometers from the Ridge and is an uphill climb through the beautiful deodar trees. The Jakhu Temple of Simla is dedicated to the monkey God, Hanuman. The Jakhu temple is located at a height of 2455 meters and is situated on the highest peak in Shimla. "
            },
            
            "Sankat Mochan Temple" : {
                "Description" : "One of the most popular tourist attractions of Shimla, the Sankat Mochan Temple is located among lush green and tranquil locales in this beautiful hill station. Shri Sankat Mochan temple in Simla is located on the Kalka - Shimla highway, on the National Highway - 22. The temple is a place where one can actually meditate and enjoy the calm and peaceful surroundings."
            },
            " Christ Church and St. Michael's Cathedral " : {
                "Description" : "The famous Christ church and St. Michael's Cathedral of Shimla is a beautiful architectural masterpiece. The construction of this church took a span of 11 years and is considered to be the second oldest church in North India. This splendid church is located just near the Ridge in Shimla and can be reached very easily from anywhere."
            }
        }
    },
    "Manali": {
        "Siteseeing": {
            " Rohtang Pass ": {
                "Description": "Rohtang Pass is the famous attractions visited place by the 25 lakh people every year. This snow-covered pass is located on the highway to Leh, and is only open from June to October every year. It lies at a height of 3979 metres, and offers spectacular views of glaciers, mountain peaks and the Chandra River."
            },

            "Solang Valley": {
                "Description": "Around 13 kilometres from Manali, this pretty valley is a popular spot for adventure sports, especially skiing. The valley offers panoramic views of the surrounding snow-capped mountains and glaciers."
            },

            "Jogni Waterfalls": {
                "Description": "If you are looking to embark on a trek to one of the most remote and untouched places to visit in Manali, then Jogni also called as Jugni Waterfalls is the best place to be. Located at an easy distance of about 3km from the main market of Manali, it can be reached directly by a vehicle. As soon as you park at the base of the falls, you will see the beautiful natural scenery that encircles the entire falls area. "
            },
            "Bhrigu Lake": {
                "Description": "Also known as the ‘Pool of Gods’, Bhrigu Lake is a majestic an picturesque water body that is located at a distance of about 43km from the town of Manali. Each year, the lake attracts thousands of trekkers and visitors who come here to appreciate the serene, crystal clear blue pools of the lake as a part of their itinerary to visit Manali tourist places. Rising to a height of about 14,100ft it is located on the eastern courses of the Rohtang Pass. One of the many glacial lakes of the Kullu Valley, it makes up for being a perfect weekend outing on your next trip to Manali. "
            },
        },

        "Restaurants": {
            "Mount View Restaurant": {
                "Description": "Among all the best restaurants in Manali, Mount View is the oldest fine dining, known for serving exquisite Chinese, Tibetan, Italian, Japanese, Continental, and Indian delicacies. If you’re a foodie at heart, this place would be your new haven!",
            },

            "Fat Plate Restaurant": {
                "Description": "Situated inside Gurung Cottages on the left bank of the Beas, this cafe is a charming little eatery. With a homely food and ambience, it lets you gorge and relax right in the lap of nature. If you’re looking for a refreshing experience, make sure you don’t miss out this place! ",
            },

            "Drifter’s Cafe": {
                "Description": "A perfect place to read a book or play fun board games with your loved ones, this cafe situated in Drifters Inn serves you more than just scrumptious food. Often home to acoustic music or karaoke nights, it lets the party begin be it any hour of the day.",
            },

            "Chopsticks Restaurant" : {
                "Description" : "One of the best restaurants in Manali mall road, Chopsticks is a foodie’s paradise. Its extensive menu of Tibetan, Indian, Chinese, and Japanese cuisines would leave you spoilt for choice. Amidst the busy street, this little restaurant might not tickle your fancy at first, but would definitely leave you smitten later."
            },
        },

        "Shopping": {
            "Old Manali Market": {
                "Description": "Perhaps the best place for shopping in Manali is the Old Manali Market which sells souvenirs, bohemian jewellery and other stuff for several foreign tourists. The prices are inflated here, so you will have to bargain real hard with the local shopkeepers here. A word of advice, If you are planning to do street shopping in Manali, keep a watchful eye for pickpockets."
            },

            "Tibetan Market" : {
                "Description" : "Walk in the delightful Tibetan Market in Manali and feel the calm and tranquil Tibetan aura that puts you in a state of relaxation. This delightful spot in Manali is yet another famous shopping destination known for its intriguing items. It is an apt place for folks who are interested in Tibetan stuff. As you walk through the narrow lanes adorned with shops on both sides, you can see a large variety of items that reminds you of the tradition and culture of Tibet. Apart from Tibetan artifacts and souvenirs, you can explore the small shops for prayer wheels, bamboo items, thangkas, silver and turquoise jewelry, home décor items and clothing."
            },

            "Himachal Emporium" : {
                "Description" : "If you are looking for local handicrafts, Himachal Handicrafts Emporium in the Mall is a good spot for you. With its beautiful and delicate goods, this place exudes a charm that’s truly fascinating. The vibrant atmosphere created by the bright and beautiful colors blend perfectly with the beauty outside. You can shop for interesting souvenirs. This government emporium is famous for its warm and colorful pashmina and woolen shawls, home décor, handmade footwear, toys, Himachali coats, rugs and so on. Their colorful caps and clothing which are mostly hand woven are quite popular among the tourists."
            },

            "Manu Market" : {
                "Description" : "The small yet bustling Manu Market is located at the Mall in Manali. This market is well known among both, locals and tourists. As you amble through the quaint-looking market, the brilliant colors of attractive goods are sure to draw your attention. You can shop here for a plethora of items like a number of books and clothes. You can also shop for handloom products, footwear, electrical appliances, crockery, utensils, artificial jewelry and a lot more. Apart from shops, you can find some restaurants that serve a variety of delicious treats and offer a relaxing ambiance. "
            },
        },

        "Religious Places": {
            "Hidimba Devi Temple": {
                "Description": "Locally known as the Dhungiri temple, Hidimba Devi temple or Hadmiba temple, it is one of the most popular temples in Manali. This four-storyed structure lies amid a forest, called Dhungiri Van Vihar.  It is an ancient cave temple dedicated to Hidimbi Devi, wife of Bhima, a figure in the Indian epic Mahābhārata. The temple is surrounded by a cedar forest at the foot of the Himālayas. The sanctuary is built over a huge rock jutting out of the ground which was worshiped as an image of the deity. The structure was built in 1553."
            },
        }
    },
    "Dehradun": {
        "Siteseeing": {
            "Lacchiwalla": {
                "Description": "Lacchiwalla is a popular picnic spot situated a short distance away from Dehradun. Located on the Haridwar-Rishikesh Road, Lacchiwalla is 22 kilometers away from Dehradun and is very popular among tourists. Easily accessible, Lacchiwalla is renowned for its lush greenery, scenic beauty and pleasant climate all through the year. Birdwatchers will especially enjoy spotting the many different species of colourful birds at Lacchiwalla"
            },

            "Robber’s Cave": {
                "Description": "A beautiful and calm spot, ideal for a picnic, Robbers Cave is a very popular tourist attraction,  Located 8 kilometers from Dehradun. Robber’s cave is famous for a strange natural occurrence; a stream of water suddenly appears and then disappears underground, only to appear again a few yards away. "
            },

            "Sahastradhara": {
                "Description": "Literally meaning “Thousand fold spring”, the Sahastradhara is a beautiful waterfall located 11 kilometers from Dehradun. Sahastradhara is famous for its sulphur water springs which contain medicinal properties and can cure skin ailments. Flanked by the Baldi River and caves on each side, Sahastradhara is renowned for its natural beauty and is a popular picnic spot. A year round destination, Sahastradhara is best visited in the monsoon when the water comes gushing off the cliff. "
            },
            "Sahastradhara Heights ": {
                "Description": "Sahastradhara Heights is a theme park located 15 kilometers from Dehradun and is a great place for the entire family to enjoy various activities. Situated opposite the Sahastradhara waterfalls, the Sahastradhara Heights is perched on top of a hill from where you can get a 360 degree view of the city of Dehradun. Sahastradhara Heights is a great place for children who can take advantage of facilities such as a virtual zoo, children’s park, rides, swings, video game parlour, wall climbing and water falls. Apart from that there are also dining, ropeway and sightseeing facilities as well."
            },
        },

        "Restaurants": {
            "Mount View Restaurant": {
                "Description": "Among all the best restaurants in Manali, Mount View is the oldest fine dining, known for serving exquisite Chinese, Tibetan, Italian, Japanese, Continental, and Indian delicacies. If you’re a foodie at heart, this place would be your new haven!",
            },

            "Fat Plate Restaurant": {
                "Description": "Situated inside Gurung Cottages on the left bank of the Beas, this cafe is a charming little eatery. With a homely food and ambience, it lets you gorge and relax right in the lap of nature. If you’re looking for a refreshing experience, make sure you don’t miss out this place! ",
            },

            "Drifter’s Cafe": {
                "Description": "A perfect place to read a book or play fun board games with your loved ones, this cafe situated in Drifters Inn serves you more than just scrumptious food. Often home to acoustic music or karaoke nights, it lets the party begin be it any hour of the day.",
            },

            "Chopsticks Restaurant" : {
                "Description" : "One of the best restaurants in Manali mall road, Chopsticks is a foodie’s paradise. Its extensive menu of Tibetan, Indian, Chinese, and Japanese cuisines would leave you spoilt for choice. Amidst the busy street, this little restaurant might not tickle your fancy at first, but would definitely leave you smitten later."
            },
        },

        "Shopping": {
            "Old Manali Market": {
                "Description": "Perhaps the best place for shopping in Manali is the Old Manali Market which sells souvenirs, bohemian jewellery and other stuff for several foreign tourists. The prices are inflated here, so you will have to bargain real hard with the local shopkeepers here. A word of advice, If you are planning to do street shopping in Manali, keep a watchful eye for pickpockets."
            },

            "Tibetan Market" : {
                "Description" : "Walk in the delightful Tibetan Market in Manali and feel the calm and tranquil Tibetan aura that puts you in a state of relaxation. This delightful spot in Manali is yet another famous shopping destination known for its intriguing items. It is an apt place for folks who are interested in Tibetan stuff. As you walk through the narrow lanes adorned with shops on both sides, you can see a large variety of items that reminds you of the tradition and culture of Tibet. Apart from Tibetan artifacts and souvenirs, you can explore the small shops for prayer wheels, bamboo items, thangkas, silver and turquoise jewelry, home décor items and clothing."
            },

            "Himachal Emporium" : {
                "Description" : "If you are looking for local handicrafts, Himachal Handicrafts Emporium in the Mall is a good spot for you. With its beautiful and delicate goods, this place exudes a charm that’s truly fascinating. The vibrant atmosphere created by the bright and beautiful colors blend perfectly with the beauty outside. You can shop for interesting souvenirs. This government emporium is famous for its warm and colorful pashmina and woolen shawls, home décor, handmade footwear, toys, Himachali coats, rugs and so on. Their colorful caps and clothing which are mostly hand woven are quite popular among the tourists."
            },

            "Manu Market" : {
                "Description" : "The small yet bustling Manu Market is located at the Mall in Manali. This market is well known among both, locals and tourists. As you amble through the quaint-looking market, the brilliant colors of attractive goods are sure to draw your attention. You can shop here for a plethora of items like a number of books and clothes. You can also shop for handloom products, footwear, electrical appliances, crockery, utensils, artificial jewelry and a lot more. Apart from shops, you can find some restaurants that serve a variety of delicious treats and offer a relaxing ambiance. "
            },
        },

        "Religious Places": {
            "Hidimba Devi Temple": {
                "Description": "Locally known as the Dhungiri temple, Hidimba Devi temple or Hadmiba temple, it is one of the most popular temples in Manali. This four-storyed structure lies amid a forest, called Dhungiri Van Vihar.  It is an ancient cave temple dedicated to Hidimbi Devi, wife of Bhima, a figure in the Indian epic Mahābhārata. The temple is surrounded by a cedar forest at the foot of the Himālayas. The sanctuary is built over a huge rock jutting out of the ground which was worshiped as an image of the deity. The structure was built in 1553."
            },
        }
    },
    "": {
        "Location 1": {
            "Name": "Leh, Ladakh",
            "Description": "The arrival of the summers is also an invitation to travellers from across the world to visit the Indian traveller's Mecca, Ladakh. The roads via Manali and Srinagar open routes to Leh, the capital city of Ladakh and the marvelous towns on the route to Leh are the sight to behold. Leh Palace, Shey Palace, Thiksey Monastery, Pangong Tso and Tso Moriri are some of the highlights that are at their scenic best during this time of the year. Travellers can choose to fly to Leh directly. Alternative road routes to reach Leh are,  From Srinagar via Zoji La,  From Manali via Tanglang La."
        },

        "Location 2": {
            "Name": "Kodaikanal",
            "Description": "Sometime in the middle of the year, take a trip to the quaint little hill station of South India, Kodaikanal. Kodai Lake, Pillar rocks and the surrounding pine forests together make Kodaikanal one of the most peaceful locations to spend your vacation. Vattakanal a nearby village is also an emerging backpacking destination. Nearest airport to Koidaikanal is Madurai. Regular bus services are also available for Kodaikanal from Chennai and Coimbatore."
        }
    },

}